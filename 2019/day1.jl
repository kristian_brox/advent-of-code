#!/usr/bin/env julia
#Reading the input and writing it to an Int array
input = readlines("input_day1")
input = parse.(Int, input)

using Test
# Day 1 first task
calcFuel(mass) = Int(floor(mass/3))-2
@test calcFuel(12) == 2
@test calcFuel(14) == 2
@test calcFuel(1969) == 654
@test calcFuel(100756) == 33583

# Calculating the data set, input
mass = sum(calcFuel,input) # Mass that is supposedly required for liftoff

#Day 1 second task
# This should calculate the required fual that is actually needed, also considering the mass of the added fuel
function calcAllFuel(fuelmass, totfuel=0)
	fuel = calcFuel(fuelmass)
	if fuel > 0
		totfuel += fuel
		calcAllFuel(fuel,totfuel) #Using recursion
	else
		return totfuel
	end
end

@test calcAllFuel(14) == 2
@test calcAllFuel(1969) == 966
@test calcAllFuel(100756) == 50346

#All fuel for all modules
sum(calcAllFuel, input)

