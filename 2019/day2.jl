#!/usr/bin/env julia
#Reading the list and writing it to an Int array
input = include("input_day2")

function opcode(list,noun=list[2],verb=list[3])
	list = collect(list)
	list[2] = noun
	list[3] = verb
	for i in 1:4:length(list)
		if list[i] == 1
			list[list[i+3]+1] = list[list[i+1]+1]+list[list[i+2]+1] # these are 1 indexed
		elseif list[i] == 2
			list[list[i+3]+1] = list[list[i+1]+1]*list[list[i+2]+1] # these are 1 indexed
		else
			return ntuple(i -> list[i], length(list))
		end
	end
end

test1 = (1,9,10,3,2,3,11,0,99,30,40,50)
answer1 = (3500,9,10,70,2,3,11,0,99,30,40,50)
test2 = (1,0,0,0,99)
answer2 = (2,0,0,0,99)
test3 = (2,3,0,3,99)
answer3 = (2,3,0,6,99)
test4 = (2,4,4,5,99,0)
answer4 = (2,4,4,5,99,9801)
test5 = (1,1,1,4,99,5,6,0,99)
answer5 = (30,1,1,4,2,5,6,0,99)

using Test
# Day 2 first task
@test opcode(test1) == answer1
@test opcode(test2) == answer2
@test opcode(test3) == answer3
@test opcode(test4) == answer4
@test opcode(test5) == answer5

#The list should be set to the state before the computer caught fire:
alteredInput = collect(input)
alteredInput = ntuple(i -> alteredInput[i], length(alteredInput))
answer1 = opcode(alteredInput,12,2)
answer1[1] # This is what is expected as the answer. It took me too long to figure out as I was attemting to answer with a tuple (1, 23 ,4...)


# Day 2 second task
#==
I need to find to inpust, noun and verb, that replaces 12 and 2 from the previous task.
The values will be in between 0 and 99
==#

function findInputs(list, output)
	for i in 0:99, j in 0:99
		out2 = opcode(list,i,j)
		if out2[1] == output
			return i,j
		end
	end
end

answer2 = findInputs(collect(input),19690720)
answer2 = answer2[1]*100 + answer2[2]
