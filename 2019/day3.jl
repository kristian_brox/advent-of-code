#!/usr/bin/env julia
#Reading a list of two wires seperated by \n
input = readlines("input_day3") # 2-element Array, seperated by comma

function splitWires(input)
	wire1 = split(input[1],",") # Take each wire and split with the comma delimiter
	wire2 = split(input[2],",") # Take each wire and split with the comma delimiter
	return [wire1,wire2]
end

# Gives the direction of the point in x and y coordinates
function coordinateXY(point)
	x = y = 0
	if point[1] == 'U'
		y = parse(Int, point[2:end])
	elseif point[1] == 'D'
		y = -parse(Int, point[2:end])
	elseif point[1] == 'R'
		x = parse(Int, point[2:end])
	else 			# Left
		x = -parse(Int, point[2:end])
	end
	return [x,y]
end

# The range of the x and y positions for a line is between the x and y values of old and new position
newpos(newdir,oldpos=[0,0]) = oldpos + coordinateXY(newdir)

#Checking where two lines cross. The lines are straight, so either the x, or the y, coordinate stays constant for the line. The lines are either horizontal or vertical. I am assuming the two lines can't cross if both are horizontal or vertical. The lines are on the form [xold, xnew, yold, ynew]
function isHor(line)
	if line[1]==line[2]
		return false
	else # I am assuming the line must be horizontal if it is not vertical
		return true
	end
end

# Returns the coordinates if the lines are crossing, returns false if not.
function crossPoint(line1, line2)
	if xor(isHor(line1),isHor(line2)) # First checking if the lines can cross, one hor and one vert line
		if isHor(line1) # line1 is horizontal, and that means line2 is vertical
			if line1[3]>=minimum(line2[3:4]) && line1[3]<=maximum(line2[3:4]) && line2[1]>=minimum(line1[1:2]) && line2[1]<=maximum(line1[1:2])
				return [line2[1],line1[3]]
			else # Not crossing
				return [0,0]
			end
		else # line1 is vertical (const x) and line2 is horizontal (const y)
			if line1[1]>=minimum(line2[1:2]) && line1[1]<=maximum(line2[1:2]) && line2[4]>=minimum(line1[3:4]) && line2[4]<=maximum(line1[3:4])
				return [line1[1],line2[3]]
			else # Not crossing
				return [0,0]
			end
		end

	else
		return [0,0]
	end
end

# Lists all the lines from old to new position [xold,xnew,yold,ynew]
function linePaths(wire)
	lines = zeros(Int64,4,length(wire))
	(xnew,ynew)=coordinateXY(wire[1])
	lines[1:end,1] = [0,xnew,0,ynew] # sets the first line
	for i in 2:length(wire)
		(xdir,ydir)=coordinateXY(wire[i])
		lines[1:end,i] = [lines[2,i-1],lines[2,i-1]+xdir,lines[4,i-1],lines[4,i-1]+ydir]
	end
	return lines
end

function shortestManhattan(wire1,wire2)
	lines1 = linePaths(wire1)
	lines2 = linePaths(wire2)
	answer = Inf
	for i in 1:length(wire1)
		for j in 1:length(wire2)
			(xcross,ycross) = crossPoint(lines1[1:end,i],lines2[1:end,j])
			manDist = abs(xcross)+abs(ycross)
			if manDist>0 && manDist<answer
				answer = manDist
			end
		end
	end
	return answer
end

# Day 3 1st task

(w1, w2)=splitWires(input)

test1w1 = "R75,D30,R83,U83,L12,D49,R71,U7,L72"
test1w2 = "U62,R66,U55,R34,D71,R55,D58,R83"
(test1w1,test1w2) = splitWires([test1w1,test1w2])
test2w1 = "R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51"
test2w2 = "U98,R91,D20,R16,D67,R40,U7,R15,U6,R7"
(test2w1,test2w2) = splitWires([test2w1,test2w2])
answer1 = 159
answer2 = 135
using Test
@test shortestManhattan(test1w1,test1w2) == answer1
@test shortestManhattan(test2w1,test2w2) == answer2

answer = shortestManhattan(w1,w2)

# Day 3 2nd task

# Returning the number of steps in x and y direction combined up to one number in the array
function stepNumber(wire,num)
	steps = 0
	for i in 1:num
		steps = steps + sum(abs.(coordinateXY(wire[i])))
	end
	return steps
end

# Returning the fewest steps combined for both wires before interception
function fewestSteps(wire1,wire2)
	lines1 = linePaths(wire1)
	lines2 = linePaths(wire2)
	wirenumber1 = 0
	wirenumber2 = 0
	steps = Inf
	for i in 1:length(wire1)
		for j in 1:length(wire2)
			(xcross,ycross) = crossPoint(lines1[1:end,i],lines2[1:end,j])
			manDist = abs(xcross)+abs(ycross)
			if manDist>0
				newSteps = stepNumber(wire1,i-1)+stepNumber(wire2,j-1) # The length of the last line is shorter
				newSteps = newSteps + sum(abs.(lines1[2:2:4,i-1]-2*[xcross,ycross]+lines2[2:2:4,j-1])) # This is the extra bit between second last line and the point. This could be more elegant..
				if steps > newSteps
					steps = newSteps
				end
			end
		end
	end
	return steps
end


answer1part2 = 610
answer2part2 = 410

@test fewestSteps(test1w1,test1w2) == answer1part2
@test fewestSteps(test2w1,test2w2) == answer2part2

answerpart2 = fewestSteps(w1,w2)
