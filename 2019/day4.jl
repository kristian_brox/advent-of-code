#!/usr/bin/env julia
# Day 4 - finding a password
input = [134564 585159]

# Function that takes a number and a position, and return the int in that position
numpos(num,pos) = parse(Int, string(num)[pos])

# Checks if to adjecent numbers are equal, and increasing left to right
function isPass(pass)
	neighbour = false
	for i in 1:5
		if numpos(pass,i)<= numpos(pass,i+1)
			if numpos(pass,i) == numpos(pass,i+1)
				neighbour = true
			end
		else
			return false
		end
	end
	if neighbour == true
		return true
	else
		return false
	end
end

# How many passwords are there?
function howMany(input)
	count = 0
	for i in input[1]:input[2]
		if isPass(i)
			count+=1
		end
	end
	return count
end


answer = howMany(input)

# Checks if to adjecent numbers are equal, and not more than two adjecent, and increasing left to right
function isPass2(pass)
	neighbour = false
	for i in 1:5
		if numpos(pass,i)<= numpos(pass,i+1)
			if i == 5 && numpos(pass,i) == numpos(pass,i+1) && numpos(pass,i) != numpos(pass,i-1)
				neighbour = true
			elseif i == 1 && numpos(pass,i) == numpos(pass,i+1) && numpos(pass,i) != numpos(pass,i+2)
				neighbour = true
			elseif 5>i>1 && numpos(pass,i) == numpos(pass,i+1) && numpos(pass,i) != numpos(pass,i+2) && numpos(pass,i) != numpos(pass,i-1)
				neighbour = true
			end

		else
			return false
		end
	end
	if neighbour == true
		return true
	else
		return false
	end
end

# How many passwords are there in part 2?
function howMany2(input)
	count = 0
	for i in input[1]:input[2]
		if isPass2(i)
			count+=1
		end
	end
	return count
end

answer2 = howMany2(input)
