#!/usr/bin/env julia
#Reading the input
input = readlines("inputday1.txt")
input = parse.(Int, input)


function is2020(input)
	for i in 1:length(input)
		for j in 2:length(input)
			if (input[i]+input[j])==2020
				return input[i]*input[j]
			end
		end
	end
end

example = [1721 979 366 299 675 1456]

using Test
# Day 1 first task
@test is2020(example) == 514579
answer = is2020(input)


function is2020three(input)
	for i in 1:length(input)
		for j in 2:length(input)
			for k in 3:length(input)
				if (input[i]+input[j]+input[k])==2020
					return input[i]*input[j]*input[k]
				end
			end
		end
	end
end

@test is2020three(example) == 241861950
answerpart2 = is2020three(input)
