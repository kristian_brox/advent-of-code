#!/usr/bin/env julia
#Reading the input
input = readlines("inputday10.txt")


function count1nd3s(input)
	input = parse.(Int64, input)

	sorted = sort(input)
	pushfirst!(sorted,0)
	dif = sorted[2:end]-sorted[1:end-1]
	return count(isequal(1),dif), count(isequal(3),dif)+1

end


answer = count1nd3s(input)
answer = answer[1]*answer[2]


function diffArr(input)
	input = parse.(Int64, input)
	sorted = sort(input)
	pushfirst!(sorted,0)
	return sorted[2:end]-sorted[1:end-1]
end


function findCom(n)
	if n == 1
		return 1
	elseif n == 2
		return 2
	elseif n == 3
		return 4
	elseif n == 4
		return 7
	elseif n == 5
		return 13
	elseif n == 6
		return 24
	elseif n == 7
		return 44
	elseif n > 7
		println("vet ikke hva denne er")
	end
end

diffa = [diffArr(input); 3]

function allCom(diffa, start=1, count = 1)
	next1 = findnext(isequal(1),diffa,start)
	if !isnothing(next1)
		start = findnext(isequal(3),diffa,next1)
	else
		start = nothing
	end

	if !isnothing(start)
		n = length(diffa[next1:start-1])
		count = findCom(n)*allCom(diffa,start)
	end
	return count
end

answer2 = allCom(diffa)
