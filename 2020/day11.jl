#!/usr/bin/env julia
#Reading the input
input = readlines("inputday11.txt")
input = split.(input,"")

function adjOcc(input, i,j)
	n = 0
	lengthx = length(input[1])
	lengthy = length(input)
	if j+1<= lengthx && input[i][j+1] == "#"
		n += 1
	end
	if j+1<= lengthx && i+1<= lengthy && input[i+1][j+1] == "#"
		n += 1
	end
	if i+1<= lengthy && input[i+1][j] == "#"
		n += 1
	end
	if j-1 > 0 && i+1<= lengthy && input[i+1][j-1] == "#"
		n += 1
	end
	if j-1 > 0 && input[i][j-1] == "#"
		n += 1
	end
	if j-1 > 0 && i-1 > 0 && input[i-1][j-1] == "#"
		n += 1
	end
	if i-1 > 0 && input[i-1][j] == "#"
		n += 1
	end
	if i-1 > 0 && j+1<= lengthx && input[i-1][j+1] == "#"
		n += 1
	end
	return n
end

function changeSeats(input)
	newSeat = deepcopy(input)
	for i in 1:length(input)
		for j in 1:length(input[1])
			if input[i][j] == "L"
				if adjOcc(input,i,j) == 0
					newSeat[i][j] = "#"
				end
			elseif input[i][j] == "#"
				if adjOcc(input,i,j) >= 4
					newSeat[i][j] = "L"
				end
			end
		end
	end
	return newSeat
end

function eqSeats(input)
	eq = false
	newSeat = deepcopy(input)
	while !eq
		newSeat2 = changeSeats(newSeat)
		if newSeat2 == newSeat
			eq = true
		else
			newSeat = deepcopy(newSeat2)
		end
	end
	return newSeat
end

# answer = sum(count.(isequal("#"),eqSeats(input)))


function adjOcc2(input, i,j)
	n = 0
	lengthx = length(input[1])
	lengthy = length(input)
	a = 1
	while j+a<= lengthx #east
		if input[i][j+a] == "#"
			n += 1
			break
		elseif input[i][j+a] == "L"
			break
		end
		a += 1
	end
	a = 1
	while j+a<= lengthx && i+a<= lengthy #south east
		if input[i+a][j+a] == "#"
			n += 1
			break
		elseif input[i+a][j+a] == "L"
			break
		end
		a += 1
	end
	a = 1
	while i+a<= lengthy #south
		if input[i+a][j] == "#"
			n += 1
			break
		elseif input[i+a][j] == "L"
			break
		end
		a += 1
	end
	a = 1
	while j-a > 0 && i+a<= lengthy #south west
		if input[i+a][j-a] == "#"
			n += 1
			break
		elseif input[i+a][j-a] == "L"
			break
		end
		a += 1
	end
	a = 1
	while j-a > 0 #west
		if input[i][j-a] == "#"
			n += 1
			break
		elseif input[i][j-a] == "L"
			break
		end
		a += 1
	end
	a = 1
	while j-a > 0 && i-a > 0 #north west
		if input[i-a][j-a] == "#"
			n += 1
			break
		elseif input[i-a][j-a] == "L"
			break
		end
		a += 1
	end
	a = 1
	while i-a > 0 #north
		if input[i-a][j] == "#"
			n += 1
			break
		elseif input[i-a][j] == "L"
			break
		end
		a += 1
	end
	a = 1
	while j+a <= lengthx && i-a > 0 #north east
		if input[i-a][j+a] == "#"
			n += 1
			break
		elseif input[i-a][j+a] == "L"
			break
		end
		a += 1
	end
	return n
end



function changeSeats2(input)
	newSeat = deepcopy(input)
	for i in 1:length(input)
		for j in 1:length(input[1])
			if input[i][j] == "L"
				if adjOcc2(input,i,j) == 0
					newSeat[i][j] = "#"
				end
			elseif input[i][j] == "#"
				if adjOcc2(input,i,j) >= 5
					newSeat[i][j] = "L"
				end
			end
		end
	end
	return newSeat
end

function eqSeats2(input)
	eq = false
	newSeat = deepcopy(input)
	while !eq
		newSeat2 = changeSeats2(newSeat)
		if newSeat2 == newSeat
			eq = true
		else
			newSeat = deepcopy(newSeat2)
		end
	end
	return newSeat
end

answer2 = sum(count.(isequal("#"),eqSeats(input)))
