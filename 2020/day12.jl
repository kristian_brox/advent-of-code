#!/usr/bin/env julia
#Reading the input
input = readlines("inputday12.txt")


function newCor(line, oldr=1)
	dir = line[1]
	num = parse(Int64, line[2:end])
	x = 0
	y = 0
	r = 0
	if dir == 'E' || oldr == 1+4
		x += num
	elseif dir == 'S' || oldr == 2+4
		y -= num
	elseif dir == 'W' || oldr == 3+4
		x -= num
	elseif dir == 'N' || oldr == 0+4
		y += num
	elseif dir == 'R'
		r = num/90
	elseif dir == 'L'
		r = -num/90
	elseif dir == 'F'
		x,y,r = newCor(line,oldr+4) # Call the function again with oldr+4. Then the correct direction for 'forward' is chosen
		r = 0
	end
	return x,y,mod(r+oldr,4)
end


function readlist(input)
	r = 1
	x = 0
	y = 0
	for i in 1:length(input)
		xnew,ynew,rnew = newCor(input[i],r)
		r = rnew
		x += xnew
		y += ynew
	end
	return x,y
end

#answer = sum(abs.(readlist(input)))

function readlist2(input)
	wp = [10,1] # Waypoint starting at 10 east (x) and 1 north (y)
	pos = [0,0] # Initial ship position

	for i in 1:length(input)
		line = input[i]
		dir = line[1]
		num = parse(Int64, line[2:end])
		if dir == 'L' || dir == 'R'
			wp = rotwp(wp,dir,num) # rotate way point is a function
		elseif dir == 'F'
			pos = [pos[1]+num*wp[1],pos[2]+num*wp[2]]
		else
			wp = newp(wp,dir,num) # new way point is a function
		end

	end
	return pos
end

function rotwp(wp,dir,num)
	rotnum = num/90
	if dir == 'R'
		for i in 1:rotnum
			wp = [wp[2],-wp[1]]
		end
	end
	if dir == 'L'
		for i in 1:rotnum
			wp = [-wp[2],wp[1]]
		end
	end
	return wp
end

function newp(wp,dir,num)
	if dir == 'N'
		wp[2] += num
	elseif dir == 'S'
		wp[2] -= num
	elseif dir == 'E'
		wp[1] += num
	elseif dir == 'W'
		wp[1] -= num
	end
	return wp
end


answer2 = sum(abs.(readlist2(input)))
