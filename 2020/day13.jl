#!/usr/bin/env julia
#Reading the input
input = readlines("inputday13.txt")

function busIDs(line)
	numberpos = findall(r"\d+",line) # Testing some regex to find the number positions
	ids = zeros(Int64,length(numberpos))
	for i in 1:length(numberpos)
		ids[i] = parse(Int64,line[numberpos[i]])
	end
	return ids
end

function earliestBus(input)
	time = parse(Int64,input[1])
	line = input[2]
	ids = busIDs(line)
	bustime = ceil.(time./ids).*ids
	idpos = findfirst(isequal(minimum(bustime)),bustime)
	return ids[idpos]
end


time = parse(Int64,input[1])
id = earliestBus(input)
wait = ceil.(time/id)*id-time
answer = id*wait*wait
