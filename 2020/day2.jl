#!/usr/bin/env julia
#Reading the input
input = readlines("inputday2.txt")

function passRange(linje)
	bindpos = 0
	førstetall = 0
	bindpos = 0
	for i in 1:length(linje)
		if linje[i]=='-'
			førstetall = parse(Int, linje[1:i-1])
			bindpos = i
		elseif linje[i]==' '
			andretall = parse(Int, linje[bindpos+1:i-1])
			return førstetall,andretall
		end
	end
end

function bokstav(linje)
	for i in 1:length(linje)
		if linje[i] == ':'
			return linje[i-1]
		end
	end
end

function passord(linje)
	for i in 1:length(linje)
		if linje[i:i+1] == ": "
			return linje[i+2:end]
		end
	end
end

function sjekkPass(linje)
	pass = passord(linje)
	freq = 0
	for i in 1:length(pass)
		if pass[i] == bokstav(linje)
			freq += 1
		end
	end
	if freq <= passRange(linje)[2] && freq >= passRange(linje)[1]
		return true
	else
		return false
	end
end

function hvorMange(input)
	freq = 0
	for i in 1:length(input)
		if sjekkPass(input[i])
			freq+=1
		end
	end
	return freq
end

answer = hvorMange(input)



function førstepos(linje)
	if bokstav(linje) == passord(linje)[passRange(linje)[1]]
		return true
	else
		return false
	end
end

function andrepos(linje)
	if bokstav(linje) == passord(linje)[passRange(linje)[2]]
		return true
	else
		return false
	end
end

function realPass(linje)
	return xor(førstepos(linje),andrepos(linje))
end

function hvorMange2(input)
	freq = 0
	for i in 1:length(input)
		if realPass(input[i])
			freq+=1
		end
	end
	return freq
end
answer = hvorMange2(input)
