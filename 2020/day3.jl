#!/usr/bin/env julia
#Reading the input
input = readlines("inputday3.txt")

function isTree(symbol)
	if symbol == '#'
		return true
	else
		return false
	end
end

function move31(xold, yold)
	return xold+3,yold+1
end



function needRepeat(xnew, len)
	if xnew > len
		return xnew-len
	else
		return xnew
	end
end

function countTrees(input)
	tree = 0
	x = 1
	y = 1
	len = length(input[1])
	for i in 1:length(input)-1
		x,y = move31(x,y)
		x = needRepeat(x,len)
		if isTree(input[y][x])
			tree += 1
		end
	end
	return tree
end

answer = countTrees(input)


function moveGeneral(xold, yold, xdir, ydir)
	return xold+xdir,yold+ydir
end


function countTrees2(input,xdir,ydir)
	tree = 0
	x = 1
	y = 1
	len = length(input[1])
	for i in 1:ydir:length(input)-1
		x,y = moveGeneral(x,y,xdir,ydir)
		x = needRepeat(x,len)
		if isTree(input[y][x])
			tree += 1
		end
	end
	return tree
end

function multiplyTrees(input)
	treesMulti = 1
	for i in 1:2:7
		trees = countTrees2(input,i,1)
		treesMulti = treesMulti*trees
	end
	return treesMulti
end
answer2 = multiplyTrees(input)*countTrees2(input,1,2)
