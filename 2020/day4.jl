#!/usr/bin/env julia
#Reading the input
input = readlines("inputday4.txt")
fields = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid", "cid"] # "cid" is ignored

# get the input file and a position, i. Returns all the info of the passport as one line, and the position start of the next passport
function passLine(input, i)
	if i > length(input)
		println("i er for stor")
		return false
	end
	line = input[i]
	i += 1
	while input[i] != ""
		line *= " "*input[i]
		i += 1
		if i > length(input)
			break
		end
	end
	return line,i+1
end

function isField(passline, field)
	if findfirst(field,passline) == nothing
		return false
	else
		return true
	end
end

function validPass(line, fields)
	if sum(isField.(line, fields[1:end-1])) == length(fields[1:end-1])
		return true
	else
		return false
	end
end

function countValid(input,fields)
	count = 0
	i = 1
	while true
		line,i = passLine(input,i)
		if validPass(line, fields)
			count += 1
		end
		if i > length(input)
			break
		end
	end
	return count
end

answer = countValid(input, fields)

function between(value, min, max)
	if max >= value >= min
		return true
	else
		return false
	end
end

function getFieldVal(line, field)
	start = findfirst(field,line)[end]+2
	stop = findfirst(" ", line[start:end])
	if stop == nothing
		return line[start:end]
	else
		return line[start:stop[1]+start-2]
	end
end

function validVal(value, field)
	eyeCol = ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"]
	min = 0
	max = 0
	if field == "byr"
		min = 1920
		max = 2002
	elseif field == "iyr"
		min = 2010
		max = 2020
	elseif field == "eyr"
		min = 2020
		max = 2030
	elseif field == "hgt"
		if value[end-1:end] == "cm"
			min = 150
			max = 193
			value = value[1:end-2]
		elseif value[end-1:end] == "in"
			min = 59
			max = 76
			value = value[1:end-2]
		else
			return false
		end
	elseif field == "hcl"
		min1 = '0'
		max1 = '9'
		min2 = 'a'
		max2 = 'f'
		if length(value)==7 && value[1] == '#'
			for i in 2:length(field)
				if between(value[i],min1,max1) || between(value[i],min2,max2)
				else
					return false

				end
			end
			return true
		else
			return false
		end
	elseif field == "ecl"
		if count(v->(v==value),eyeCol)==1
			return true
		else
			return false
		end
	elseif field == "pid"
		if count(i->('9'>=i>='0'),value) == 9
			return true
		else
			return false
		end
	end
	if between(parse(Int,value),min,max)
		return true
	else
		return false
	end
end


function validPass2(line, fields)
	if sum(isField.(line, fields[1:end-1])) == length(fields[1:end-1])
		values = getFieldVal.(line,fields[1:end-1])
		if (count(i->(validVal(values[i],fields[i])),1:length(fields)-1)==7)
			return true
		else
			return false
		end
	else
		return false
	end
end


function countValid2(input,fields)
	count = 0
	i = 1
	while true
		line,i = passLine(input,i)
		if validPass2(line, fields)
			count += 1
		end
		if i > length(input)
			break
		end
	end
	return count
end

answer2 = countValid2(input,fields)
