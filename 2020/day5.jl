#!/usr/bin/env julia
#Reading the input
input = readlines("inputday5.txt")

function boolVal(bool,i)
	if bool == 'B' || bool == 'R'
		return 2^i
	else
		return 0
	end
end

function getRow(line)
	row = 0
	for i in 0:6
		row+=boolVal(line[i+1],6-i)
	end
	return row
end

function getCol(line)
	col = 0
	for i in 7:length(line)-1
		col+=boolVal(line[i+1],9-i)
	end
	return col
end

function getID(line)
	return 8*getRow(line)+getCol(line)
end

answer = maximum(getID.(input))

function missingSeats(input)
	seats = getID.(input)
	seats = sort(seats)
	for i in 2:length(seats)
		if seats[i] != seats[i-1]+1
			println(seats[i])
		end
	end
	return empty
end

