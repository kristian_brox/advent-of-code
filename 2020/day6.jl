#!/usr/bin/env julia
#Reading the input
input = readlines("inputday6.txt")


function groupLine(input, i)
	line = input[i]
	i += 1
	if i <= length(input)
		while input[i] != ""
			line *= input[i]
			i += 1
			if i > length(input)
				break
			end
		end
	end
	return line,i+1
end

function groupSum(line)
	return length(unique(line))
end

function groupAll(input,i=1,sum = 0)
	line,i = groupLine(input,i)
	nsum = groupSum(line)
	if i < length(input)
		groupAll(input,i,sum+nsum)
	else
		return sum+nsum
	end
end

answer = groupAll(input)

function isCommon(line1,line2)
	line = ""
	for i in 1:length(line1)
		if !isnothing((findfirst(line1[i],line2)))
			line *= line1[i]
		end
	end
	return line
end

function commonGroup(input, i)
	throw,nexti = groupLine(input, i)
	if nexti == i+2 || i == length(input)
		return throw, nexti
	else
		line = isCommon(input[i],input[i+1])
		for j in 2:length(input[i:nexti-2])
			line = isCommon(line,input[i+j-1])
		end
	end
	return line, nexti
end


function groupAll2(input,i=1,sum = 0)
	line,newi = commonGroup(input,i)
	nsum = length(line)
	if newi <= length(input)
		groupAll2(input,newi,sum+nsum)
	else
		return sum+nsum
	end
end

answer2 = groupAll2(input)
