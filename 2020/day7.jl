#!/usr/bin/env julia
#Reading the input
input = readlines("inputday7.txt")

function outerBag(line)
	a = findfirst(' ',line)
	adj = line[1:a-1]
	b = findfirst(' ', line[a+1:end])
	color = line[a+1:a+b-1]
	return adj, color
end

function innerBag(line, numstart)

	if !isnothing(findfirst("no other", line))
		return false
	end

	adjstart = findnext(' ', line, numstart) + 1

	num = line[numstart:adjstart-2]

	colstart = findnext(' ', line, adjstart) + 1

	adj = line[adjstart:colstart-2]

	colend = findnext(' ', line, colstart)-1

	col = line[colstart:colend]

	nextbag = false
	nextbagnum = findnext(", ", line, colend)
	if !isnothing(nextbagnum)
		nextbag = nextbagnum[end]+1
	end

	return adj, col, num, nextbag
end


function outerOfinner(line, adj, col)
	# numstart for first inner bag
	numstart = findnext("contain ", line, 1)[end] + 1
	if innerBag(line,numstart) == false
		return false
	end
	adjective, color, num, nextnum = innerBag(line, numstart)

	if adjective == adj && color == col
		return outerBag(line)
	end

	while nextnum != false

		adjective, color, num, nextnum = innerBag(line, nextnum)

		if adjective == adj && color == col
			return outerBag(line)
		end
	end
	return false
end

function countOuter(input, adj, col, list = "")
	for i in 1:length(input)
		line = input[i]
		if outerOfinner(line, adj, col) != false
			adjnew, colnew = outerOfinner(line, adj, col)
			if isnothing(findfirst(adjnew*","*colnew,list))
				list *= adjnew*","*colnew*" "
			end
			list = countOuter(input, adjnew, colnew,list)
		end
	end
	return list
end


answer = count(",",countOuter(input, "shiny", "gold"))

# Part two

function outerBagLine(input, adj, col)
	for i in 1:length(input)
		line = input[i]
		adjc, colc = outerBag(line)
		if adj == adjc && col == colc
			return line
		end
	end
	return false
end

function countInner(input, adj, col, bagnum=0)
	line = outerBagLine(input, adj, col)

	numstart = findnext("contain ", line, 1)[end] + 1
	if innerBag(line,numstart) == false
		return 0
	end

	adjective, color, num, nextnum = innerBag(line, numstart)
	num = parse(Int64, num)

	bagnum = bagnum + num + num*countInner(input,adjective,color)

	while nextnum != false
		adjective, color, num, nextnum = innerBag(line, nextnum)
		num = parse(Int64, num)
		bagnum = bagnum + num + num*countInner(input,adjective,color)
	end
	return bagnum
end

answer2 = countInner(input, "shiny","gold")
