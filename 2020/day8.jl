#!/usr/bin/env julia
#Reading the input
input = readlines("inputday8.txt")


function lineInfo(line)
	acc = 0
	num = parse(Int64, line[4:end])
	nop = 0

	if line[1] == 'a'
		acc = num
		num = 1
	# elseif line[1] == 'j' # This is not neccessary. For jump, num, acc, and nop will remain
	elseif line[1] == 'n'
		nop = num
		num = 1
	end
	return num, acc, nop
end

function findLoop(input)
	loop = false
	lines = [0]

	pos = 1
	nextpos = 1
	acc = 0
	nextacc = 0
	nop = 0
	nextnop = 0

	while loop == false
		line = input[pos]
		push!(lines, pos)
		nextpos, nextacc, nextnop = lineInfo(line)
		if !isnothing(findfirst(isequal(nextpos+pos),lines))
			loop = true
		end
		pos += nextpos
		acc += nextacc
		if pos>length(input)
			return 0, acc
		end
	end
	return pos, acc
end

answer = findLoop(input)[2]

function findLoop2(input)
	loop = false
	lines = [0]
	nops = [0]
	jmps = [0]

	pos = 1
	nextpos = 1
	acc = 0
	nextacc = 0
	nop = 0
	nextnop = 0

	while loop == false
		line = input[pos]
		if line[1:3] == "nop"
			push!(nops,pos)
		elseif line[1:3] == "jmp"
			push!(jmps,pos)
		end
		push!(lines, pos)
		nextpos, nextacc, nextnop = lineInfo(line)
		if !isnothing(findfirst(isequal(nextpos+pos),lines))
			loop = true
		end
		pos += nextpos
		acc += nextacc
		if pos>length(input)
			return pos,acc,0,0
		end
	end
	return pos, acc, jmps, nops
end

function findMistake(input)
	pos, acc, jmps, nops = findLoop2(input)

	for i in 2:length(nops)
		testinput = copy(input)
		testinput[nops[i]] = replace(testinput[nops[i]],"nop"=>"jmp")
		out = findLoop(testinput)
		if out[1] == 0
			return out
		end
	end

	for i in 2:length(jmps)
		testinput = copy(input)
		testinput[jmps[i]] = replace(testinput[jmps[i]],"jmp"=>"nop")
		out = findLoop(testinput)
		if out[1] == 0
			return out
		end
	end

	return false
end

answer2 = findMistake(input)[2]
