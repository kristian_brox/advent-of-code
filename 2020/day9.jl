#!/usr/bin/env julia
#Reading the input
input = readlines("inputday9.txt")

function isSum(num, arr)
	for i in 1:length(arr)
		for j in 1:length(arr)
			if arr[i]+arr[j] == num && i != j
				return i,j
			end
		end
	end
	return false
end

function findFalse(input, preamble)
	for i in preamble+1:length(input)
		num = parse(Int64,input[i])
		arr = parse.(Int64, input[i-preamble:i-1])
		if isSum(num,arr)[1] == 0
			return i, input[i]
		end
	end
	return false
end

answer = findFalse(input,25)[2]

function sumRange(input, len, start)
	range = parse.(Int64, input[start:start+len-1])
	s = sum(range)
	return range,s
end

function findRightSum(input, inNum)
	for i in 2:length(input) # number of contiguous lines
		contig = i
		for j in 1:length(input)-contig # start in sumRange
			range,s = sumRange(input,contig, j)
			if s == inNum
				return range, j, contig
			end
		end
	end
	return false
end

inNum = parse(Int64,answer)

answer2 = findRightSum(input, inNum)[1]
answer2 = minimum(answer2)+maximum(answer2)
