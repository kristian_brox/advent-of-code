#!/usr/bin/env julia
#Reading the input
input = parse.(Int,readlines("inputday01.txt"))

function numIncreased(num, in)
	for i in 1:length(in)-1
		if in[i+1]>in[i]
			num += 1
		end
	end
	return num
end


inc = 0
answer1 = numIncreased(inc,input)

function slidingSum(num,in)
	n = length(in)
	for i in 4:n
		if sum(in[i-3:i-1])<sum(in[i-2:i])
			num += 1
		end
	end
	return num
end

inc2 = 0
answer2 = slidingSum(inc2,input)
