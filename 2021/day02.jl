#!/usr/bin/env julia
#Reading the input
input = readlines("inputday02.txt")

function newDirection(command)
	x = 0
	y = 0
	dir = command[1]
	num = parse(Int,match(r"[0-9]+",command).match)
	if dir == 'f'
		x += num
	elseif dir == 'd'
		y += num
	elseif dir == 'u'
		y -= num
	end
	return x,y
end

function finalDir(commands,x=0,y=0)
	for i in 1:length(commands)
		a,b = newDirection(commands[i])
		x += a
		y += b
	end
	return x,y
end

x,y = finalDir(input)

answer1 = x*y

function newDirection2(command,x=0,y=0,aim=0)
	dir = command[1]
	num = parse(Int,match(r"[0-9]+",command).match)
	if dir == 'f'
		x += num
		y += aim*num
	elseif dir == 'd'
		aim += num
	elseif dir == 'u'
		aim -= num
	end
	return x,y,aim
end

function finalDir2(commands, x=0, y=0, aim=0)
	for i in 1:length(commands)
		x,y,aim = newDirection2(commands[i],x,y,aim)
	end
	return x,y
end

x,y = finalDir2(input)
answer2 = x*y
