#!/usr/bin/env julia
#Reading the input
input = readlines("inputday03.txt")
#input = reduce(vcat, permutedims.(collect.(input)))
#

function binCol(input, col)
	h = length(input)
	out = zeros(Int,1,h)
	for i in 1:h
		out[i] = parse(Int,input[i][col])
	end
	return out
end

function gammaRate(input)
	l = length(input[1])
	h = length(input)
	out = zeros(Int,l)
	for i in 1:l
		isones = count(x -> x == 1, binCol(input,i))
		if isones > h/2
			out[i] = 1
		end
	end
	return out
end

function epsilonRate(gammarate)
	 return ones(Int,length(gammarate)).- gammarate
end

γ = gammaRate(input)
ϵ = epsilonRate(γ)
answer = parse(Int, "0b"*join(γ))*parse(Int, "0b"*join(ϵ))
#--------part 2------------

a = binCol(input,1)
collect(a)
b = findall(x -> x==1,a)
b[1][1]
