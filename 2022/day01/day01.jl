#!/usr/bin/env julia
#Reading the input
input = readlines("input.txt")
example = readlines("example.txt")

function elfSum(input)
    elfcal = [0]
    elfnum = 1
    for i in 1:length(input)
        if input[i]!=""
            elfcal[elfnum]+=parse(Int,input[i])

        else
            elfnum += 1
            append!(elfcal,0)
        end
    end
    return elfcal
end

allelves=elfSum(input)
findmax(allelves)


function topthree(input)
    first = findmax(input)
    deleteat!(input,first[2])
    second = findmax(input)
    deleteat!(input,second[2])
    third = findmax(input)
    return first[1]+second[1]+third[1]
end

topthree(allelves)
