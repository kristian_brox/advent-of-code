#!/usr/bin/env julia
#Reading the input
input = readlines("input.txt")
example = readlines("example.txt")

function matchScore(player1, player2)
	score = 0
	if player2 == 'X' # Stone
		score += 1
		if player1 == 'A' # Draw
			score += 3
		elseif player1 == 'B' # Loose
			score += 0
		else 		      # Win
			score += 6
		end
	elseif player2 == 'Y' # Paper
		score += 2
		if player1 == 'A' # Win
			score += 6
		elseif player1 == 'B' # Draw
			score += 3
		else 		      # Loose
			score += 0
		end
	else 		      # Scissor
		score += 3
		if player1 == 'A' # Loose
			score += 0
		elseif player1 == 'B' # Win
			score += 6
		else 		      # Draw
			score += 3
		end
	end
	return score
end

matchScore(example[1][1],example[1][3])

function scoreFromList(input)
	score = 0
	for i in 1:length(input)
		score += matchScore(input[i][1],input[i][3])
	end
	return score
end

scoreFromList(input)

function scoreFromRightList(input)
	score = 0
	newHand = "XYZ"
	elfHand = "ABC"
	for i in 1:length(input)
		score += matchScore(input[i][1], newHand[mod(findfirst(input[i][3], newHand)+mod(findfirst(input[i][1],elfHand),3),3)+1])
	end
	return score
end


scoreFromRightList(input)
