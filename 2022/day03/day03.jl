#!/usr/bin/env julia
#Reading the input
input = readlines("input.txt")
example = readlines("example.txt")

function compareCompartments(line)
	len = Int(length(line)/2)
	for i in 1:len
		match = findfirst(line[i],line[(1+len):end])
		if !(isnothing(match))
			return line[i]
		end
	end
end

function priority(character)
	a = 'a':'z'
	A = 'A':'Z'
	prilist = [a;A]

	for i in 1:length(prilist)
		if prilist[i] == character
			return i
		end
	end
end

function priSum(list)
	pri = 0
	for i in 1:length(list)
		pri += priority(compareCompartments(list[i]))
	end
	return pri
end

priSum(input)

function sumBadge(input)
	sum = 0
	for i in 3:3:length(input)
		nottaken = ['a':'z';'A':'Z']
		for j in 1:length(input[i])
			char = input[i][j]
			if !isnothing(findfirst(char,input[i-1]))&&!isnothing(findfirst(char,input[i-2]))
				for k in 1:length(nottaken)
					if char == nottaken[k]
						sum += priority(char)
						deleteat!(nottaken, k)
						push!(nottaken, '0')
					end
				end
			end
		end
	end
	return sum
end


sumBadge(input)
