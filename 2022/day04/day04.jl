#!/usr/bin/env julia
#Reading the input
input = readlines("input.txt")
example = readlines("example.txt")
#Should maybe import with seperating by "-" and ","?

function getNumbers(line)
	start = 1
	fir = line[start:findfirst('-',line)-1]
	start += length(fir)+1
	second = line[start:findfirst(',',line)-1]
	start += length(second)+1
	third = line[start:findlast('-',line)-1]
	start += length(third)+1
	fourth = line[start:end]

	return parse.(Int,[fir, second, third, fourth])
end


function isIn(numbers)
	if !xor(numbers[1]<=numbers[3],numbers[2] >= numbers[4])
		return true
	elseif !xor(numbers[1]>=numbers[3],numbers[2] <= numbers[4])
		return true
	else
		return false
	end
end

function countIn(input)
	count = 0
	for i in 1:length(input)
		if isIn(getNumbers(input[i]))
			count += 1
		end
	end
	return count
end

countIn(input)

function isOverlap(numbers)
	if (!xor(numbers[2] >=numbers[3] , numbers[2] <= numbers[4])) || (!xor(numbers[1] >=numbers[3] , numbers[1] <= numbers[4]))
		return true
	elseif (!xor(numbers[4] >=numbers[1] , numbers[4] <= numbers[2])) || (!xor(numbers[3] >=numbers[1] , numbers[3] <= numbers[2]))
		return true
	end
	return false
end

n = getNumbers.(input)
count(isOverlap.(n))

